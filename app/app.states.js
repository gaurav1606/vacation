"use strict";
var place_component_1 = require('./place/place.component');
var bedroom_component_1 = require('./bedroom/bedroom.component');
var bathroom_component_1 = require('./bathroom/bathroom.component');
var location_component_1 = require('./location/location.component');
var amenities_component_1 = require('./amenities/amenities.component');
var spaces_component_1 = require('./spaces/spaces.component');
exports.appStates = [
    // 1st State
    { name: 'place', url: '/place', component: place_component_1.PlaceComponent },
    // 2nd State
    { name: 'bedroom', url: '/bedroom', component: bedroom_component_1.BedroomComponent },
    // 3rd State
    { name: 'bathroom', url: '/bathroom', component: bathroom_component_1.BathroomComponent },
    // 4th State
    { name: 'location', url: '/location', component: location_component_1.LocationComponent },
    // 5th State
    { name: 'amenities', url: '/amenities', component: amenities_component_1.AmenitiesComponent },
    //6th state
    { name: 'spaces', url: '/spaces', component: spaces_component_1.SpacesComponent }
];
//# sourceMappingURL=app.states.js.map