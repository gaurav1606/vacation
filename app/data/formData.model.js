"use strict";
var FormData = (function () {
    function FormData() {
        this.property_type = '';
        this.property_size = '';
        this.property_location = '';
        this.property_kind = '';
        this.guest_rooms = '';
        this.personal_belonging = '';
        this.guest_capacity = '';
        this.bed_foruse = '';
        this.bathrooms = '';
        this.street = '';
        this.city = '';
        this.state = '';
        this.zip = '';
    }
    return FormData;
}());
exports.FormData = FormData;
//# sourceMappingURL=formData.model.js.map