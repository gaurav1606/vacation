export class FormData {
 property_type : string='';
 property_size:string='';
 property_location:string='';
 property_kind:string='';
 guest_rooms:string='';
 personal_belonging='';
 guest_capacity='';
 bed_foruse='';
 bathrooms='';
 street: string = '';
 city: string = '';
 state: string = '';
 zip: string = '';
 amenities_essential:string;
 amenities_wifi:string;
 amenities_closet:string;
 amenities_shampoo:string;
 amenities_tv:string;
 amenities_heat:string;
 amenities_ac:string;
 amenities_breakfast:string;
 amenities_smoke:string;
 amenities_first_aid_kit:string;
 amenities_fire:string;
 amenities_safety_card:string;

 spaces_pool:string;
 spaces_kitchen:string;
 spaces_dryer:string;
 spaces_washer:string;
 spaces_parking:string;
 spaces_elevator:string;

 
}