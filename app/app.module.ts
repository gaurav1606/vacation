import { NgModule }           from '@angular/core';
import { BrowserModule }      from '@angular/platform-browser';
import { UIRouterModule }     from "ui-router-ng2";
import { FormsModule }        from '@angular/forms';
 
/* App Root */
import { AppComponent }       from './app.component';

 
/* Feature Components */
import { PlaceComponent }  from './place/place.component';
import { BedroomComponent }      from './bedroom/bedroom.component';
import { BathroomComponent }   from './bathroom/bathroom.component';
import { LocationComponent }    from './location/location.component';
import { AmenitiesComponent } from './amenities/amenities.component';
import { SpacesComponent }    from './spaces/spaces.component';

 
/* App Router */
import { UIRouterConfigFn }   from "./app.router";
import { appStates }          from "./app.states";
 
/* Shared Service */
import { FormDataService }    from './data/formData.service'
 
@NgModule({
    imports:      [ BrowserModule, 
                    FormsModule,
                    UIRouterModule.forRoot({ 
                      states: appStates,
                      useHash: true,
                      config: UIRouterConfigFn
                    }) 
                  ],
    providers:    [{ provide: FormDataService, useClass: FormDataService }],
    declarations: [ AppComponent, PlaceComponent, BedroomComponent, BathroomComponent, LocationComponent, AmenitiesComponent, SpacesComponent],
    bootstrap:    [ AppComponent ]
})
 
export class AppModule {}