import { PlaceComponent }  from './place/place.component';
import { BedroomComponent }      from './bedroom/bedroom.component';
import { BathroomComponent }   from './bathroom/bathroom.component';
import { LocationComponent }    from './location/location.component';
import { AmenitiesComponent } from './amenities/amenities.component';
import { SpacesComponent }   from './spaces/spaces.component';

 
export const appStates = [
    // 1st State
    { name: 'place', url: '/place',  component: PlaceComponent },
    // 2nd State
    { name: 'bedroom', url: '/bedroom',  component: BedroomComponent },
    // 3rd State
    { name: 'bathroom', url: '/bathroom',  component: BathroomComponent },
    // 4th State
    { name: 'location', url: '/location',  component: LocationComponent },

    // 5th State

    {name:'amenities',url:'/amenities',component:AmenitiesComponent},

    //6th state

    {name:'spaces',url:'/spaces',component:SpacesComponent}
];