import { Component, OnInit, Input, OnDestroy }   from '@angular/core';

import { FormDataService }                       from '../data/formData.service';
declare var $:any;
@Component ({
    selector:     'mt-wizard-spaces'
    ,templateUrl: 'app/spaces/spaces.component.html'
})

export class SpacesComponent implements OnInit, OnDestroy {
    title = 'What spaces can guest use?';
    @Input() formData;
    
    constructor(private formDataService: FormDataService) {
    }

    ngOnInit() {
        this.formData = this.formDataService.getData();
        console.log('Address feature loaded!');
        $(".selectpicker").selectpicker();
    }

    ngOnDestroy() {
        this.formDataService.setData(this.formData);
    }
    ngAfterViewInit() {
       $(".selectpicker").selectpicker();
    }
}