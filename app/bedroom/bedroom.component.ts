import { Component, OnInit, Input }   from '@angular/core';

import { FormDataService }            from '../data/formData.service';
declare var $:any;
@Component ({
    selector:     'mt-wizard-bedroom'
    ,templateUrl: 'app/bedroom/bedroom.component.html'
})

export class BedroomComponent implements OnInit {
    title = 'Where’s your place located?';
    @Input() formData;
    
    constructor(private formDataService: FormDataService) {
    }

    ngOnInit() {
        this.formData = this.formDataService.getData();
        console.log('bedroom feature loaded!');
       
    }
    ngAfterViewInit() {
       $(".selectpicker").selectpicker();
    }
}
