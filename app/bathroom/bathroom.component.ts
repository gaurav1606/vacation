import { Component, OnInit, Input, OnDestroy }   from '@angular/core';

import { FormDataService }                       from '../data/formData.service';
declare var $:any;
@Component ({
    selector:     'mt-wizard-address'
    ,templateUrl: 'app/bathroom/bathroom.component.html'
})

export class BathroomComponent implements OnInit, OnDestroy {
    title = 'How many guests can your place accommodate?';
    @Input() formData;
    
    constructor(private formDataService: FormDataService) {
    }

    ngOnInit() {
        this.formData = this.formDataService.getData();
        console.log('Address feature loaded!');
      
    }

    ngOnDestroy() {
        this.formDataService.setData(this.formData);
    }
    ngAfterViewInit() {
       $(".selectpicker").selectpicker();
    }
}