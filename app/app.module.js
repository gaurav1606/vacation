"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var core_1 = require('@angular/core');
var platform_browser_1 = require('@angular/platform-browser');
var ui_router_ng2_1 = require("ui-router-ng2");
var forms_1 = require('@angular/forms');
/* App Root */
var app_component_1 = require('./app.component');
/* Feature Components */
var place_component_1 = require('./place/place.component');
var bedroom_component_1 = require('./bedroom/bedroom.component');
var bathroom_component_1 = require('./bathroom/bathroom.component');
var location_component_1 = require('./location/location.component');
var amenities_component_1 = require('./amenities/amenities.component');
var spaces_component_1 = require('./spaces/spaces.component');
/* App Router */
var app_router_1 = require("./app.router");
var app_states_1 = require("./app.states");
/* Shared Service */
var formData_service_1 = require('./data/formData.service');
var AppModule = (function () {
    function AppModule() {
    }
    AppModule = __decorate([
        core_1.NgModule({
            imports: [platform_browser_1.BrowserModule,
                forms_1.FormsModule,
                ui_router_ng2_1.UIRouterModule.forRoot({
                    states: app_states_1.appStates,
                    useHash: true,
                    config: app_router_1.UIRouterConfigFn
                })
            ],
            providers: [{ provide: formData_service_1.FormDataService, useClass: formData_service_1.FormDataService }],
            declarations: [app_component_1.AppComponent, place_component_1.PlaceComponent, bedroom_component_1.BedroomComponent, bathroom_component_1.BathroomComponent, location_component_1.LocationComponent, amenities_component_1.AmenitiesComponent, spaces_component_1.SpacesComponent],
            bootstrap: [app_component_1.AppComponent]
        }), 
        __metadata('design:paramtypes', [])
    ], AppModule);
    return AppModule;
}());
exports.AppModule = AppModule;
//# sourceMappingURL=app.module.js.map