import { Component, OnInit, Input }   from '@angular/core';

import { FormDataService }            from './data/formData.service';

@Component ({
    selector:     'my-step'
    ,template: `<ui-view></ui-view>
    <pre style="position:relative">{{ formData | json }}</pre>`
})


export class AppComponent implements OnInit {
    title = 'Multi-Step Wizard';
    @Input() formData;
    
    constructor(private formDataService: FormDataService) {
    }

    ngOnInit() 
    {
        this.formData = this.formDataService.getData();
        console.log(this.title + ' loaded!');
        console.log(this.formData);
    }
}