import { Component, OnInit, Input, OnDestroy }   from '@angular/core';

import { FormDataService }                       from '../data/formData.service';
declare var $:any;
@Component ({
    selector:     'mt-wizard-amenities'
    ,templateUrl: 'app/amenities/amenities.component.html'
})

export class AmenitiesComponent implements OnInit, OnDestroy {
    title = 'What amenities do you offer?';
    @Input() formData;
    
    constructor(private formDataService: FormDataService) {
    }

    ngOnInit() {
        this.formData = this.formDataService.getData();
        console.log('Address feature loaded!');
   
    }

    ngOnDestroy() {
        this.formDataService.setData(this.formData);
    }
    ngAfterViewInit() {
       $(".selectpicker").selectpicker();
    }
}