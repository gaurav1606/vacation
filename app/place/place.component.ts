import { Component, OnInit, Input, OnDestroy }   from '@angular/core';

import { FormData }                              from '../data/formData.model';
import { FormDataService }                       from '../data/formData.service';
declare var $:any;
@Component ({
    selector:     'mt-wizard-place'
    ,templateUrl: 'app/place/place.component.html'
})

export class PlaceComponent implements OnInit, OnDestroy {
    title = 'What kind of place do you have?';
    @Input() formData: FormData;
    
    constructor(private formDataService: FormDataService) {
        
    }

    ngOnInit() {
        this.formData = this.formDataService.getData();
        console.log('Place feature loaded!');
        console.log(this.formData);
        
    }

    ngOnDestroy() {
        this.formDataService.setData(this.formData);
    }
    ngAfterViewInit() {
       $(".selectpicker").selectpicker();
         
    }
}
