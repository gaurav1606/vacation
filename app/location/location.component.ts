import { Component, OnInit, Input }   from '@angular/core';

import { FormDataService }            from '../data/formData.service';
declare var $:any;
@Component ({
    selector:     'mt-wizard-location'
    ,templateUrl: 'app/location/location.component.html'
})

export class LocationComponent implements OnInit {
    title = 'Where’s your place located?';
    @Input() formData;
    
    constructor(private formDataService: FormDataService) {
    }

    ngOnInit() {
        this.formData = this.formDataService.getData();
        console.log('location feature loaded!');
        
    }
    ngAfterViewInit() {
       $(".selectpicker").selectpicker();
    }
     changeTower() {
    setTimeout(()=>{
      alert( 'hi');
    },100);
  }
      
}
